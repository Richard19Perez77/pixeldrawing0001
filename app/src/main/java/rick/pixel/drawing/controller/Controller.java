package rick.pixel.drawing.controller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import rick.pixel.drawing.R;

public class Controller {

    /**
     * store the screen size for scaling objects
     */
    public int screenWidth, screenHeight;
    int centerX;

    Rect rect = new Rect();
    Paint paint = new Paint();

    private long time = 0;

    private int primaryColor;
    private Paint primaryPaint;
    private Paint primaryDarkPaint;
    private Paint accentPaint;
    private Paint paintWhite;

    private int primaryColorDark;
    private int primaryColorAccent;

    private int textSize;

    public void init(Context context) {
        primaryColor = ContextCompat.getColor(context, R.color.colorPrimary);
        primaryColorDark = ContextCompat.getColor(context, R.color.colorPrimaryDark);
        primaryColorAccent = ContextCompat.getColor(context, R.color.colorAccent);
        primaryPaint = new Paint();
        primaryPaint.setColor(primaryColor);
        primaryDarkPaint = new Paint();
        primaryDarkPaint.setColor(primaryColorDark);
        accentPaint = new Paint();
        accentPaint.setColor(primaryColorAccent);
        paintWhite = new Paint();
        paintWhite.setColor(Color.WHITE);
        paintWhite.setTextAlign(Paint.Align.CENTER);
    }

    /**
     * Calling draw will hand the work off to the Draw class that is implemented
     * at that time.
     *
     * @param canvas our drawing surface's canvas.
     */
    public void draw(Canvas canvas) {
        canvas.drawRect(rect, paint);
        String text = "Time: " + time;
        canvas.drawText(text, centerX, textSize * 2, paintWhite);
    }

    /**
     * On start of the application we can set the screen height and widtht.
     *
     * @param holder our surface holder object.
     * @param height height of our screen in pixels.
     * @param width  width of our screen in pixels.
     */
    public void surfaceChanged(SurfaceHolder holder, int height, int width) {
        // called when the surface is created for the thread
        screenHeight = height;
        screenWidth = width;

        // place targets
        initRects();
        initTextArea();
    }

    private void initTextArea() {
        centerX = screenWidth / 2;
        textSize = screenHeight / 20;
        paintWhite.setTextSize(textSize);
    }

    /**
     * If the application is in a state to accept touch events, handle them
     * here. Returning false means until the user presses down the handling is
     * over, returning true means the touch events will keep being fed to here.
     */
    public boolean onTouch(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            paint = getRandPaint();
            return false;
        }
        return true;
    }

    private Paint getRandPaint() {
        long time = System.currentTimeMillis();
        switch ((int) (time % 3)) {
            case 0:
                return primaryPaint;
            case 1:
                return primaryDarkPaint;
            case 2:
            default:
                return accentPaint;
        }
    }

    /**
     * Update the current objects, reset after screen cycle has ended.
     */
    public void updatePhysics() {
        time++;
    }

    private void initRects() {
        rect.top = 0;
        rect.left = 0;
        rect.right = screenWidth;
        rect.bottom = screenHeight;
    }

    /**
     * The user may have pressed the menu restart button this resets the factory
     * pattern created classes for physics and draw methods.
     */
    public void menuRestart() {
        time = 0;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long t) {
        time = t;
    }
}