package rick.pixel.drawing.surface;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import rick.pixel.drawing.R;

public class SurfaceFragment extends Fragment {

    /**
     * Will be used to hold the pixels to be drawn on.
     */
    public DrawingSurface drawingSurface;

    public SurfaceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SurfaceFragment.
     */
    public static SurfaceFragment newInstance() {
        return new SurfaceFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    private void getSharedPrefs() {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(
                "sharedPreferences", Context.MODE_PRIVATE);

        if (sharedpreferences.contains("time")) {
            drawingSurface.setTime(sharedpreferences.getLong("time", 0));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_surface, container, false);
        // get reference to our surface
        drawingSurface = (DrawingSurface) view.findViewById(R.id.drawing_surface);
        drawingSurface.setTextView((TextView) view.findViewById(R.id.pause_message));
        drawingSurface.init();

        if (savedInstanceState != null) {
            drawingSurface.setTime(savedInstanceState.getLong("time"));
        }

        getSharedPrefs();

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("time", drawingSurface.getTime());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.restart:
                drawingSurface.menuRestart();
                return true;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        startAnimation();
        drawingSurface.onResume();
    }

    private void startAnimation() {
        // start the application out by modifying the view and don't accept user
        // input until the animation ends

        //animation has been not working on all devices... may need to remove
        //on start of app and use on restart button press or whatever you like
        Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.intro);
        anim.reset();
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // if the animation isn't finished we shouldn't be drawing on it
                drawingSurface.introFinished = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // this animation is longest and touch should be ready when this
                // completes
                drawingSurface.introFinished = true;
            }
        });

        Animation anim2 = AnimationUtils.loadAnimation(getContext(), R.anim.warp_in);
        anim2.reset();

        drawingSurface.clearAnimation();
        //drawingSurface.startAnimation(anim);
        drawingSurface.introFinished = true;

        drawingSurface.messageTextView.clearAnimation();
        drawingSurface.messageTextView.startAnimation(anim2);

    }

    @Override
    public void onPause() {
        super.onPause();
        drawingSurface.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(
                "sharedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong("time", drawingSurface.getTime());
        editor.apply();
    }
}